package Task3_MyReadWriteLock;

import java.util.Collection;
import java.util.concurrent.locks.*;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 21 Dec 2018
 * Create your own ReadWriteLock (or at least simple Lock)
 */
public class MyReadWriteLock extends ReentrantReadWriteLock {

    public MyReadWriteLock() {
        super();
    }

    public MyReadWriteLock(boolean fair) {
        super(fair);
    }

    @Override
    public WriteLock writeLock() {
        return super.writeLock();
    }

    @Override
    public ReadLock readLock() {
        return super.readLock();
    }

    @Override
    protected Thread getOwner() {
        return super.getOwner();
    }

    @Override
    public int getReadLockCount() {
        return super.getReadLockCount();
    }

    @Override
    public boolean isWriteLocked() {
        return super.isWriteLocked();
    }

    @Override
    public boolean isWriteLockedByCurrentThread() {
        return super.isWriteLockedByCurrentThread();
    }

    @Override
    public int getWriteHoldCount() {
        return super.getWriteHoldCount();
    }

    @Override
    public int getReadHoldCount() {
        return super.getReadHoldCount();
    }

    @Override
    protected Collection<Thread> getQueuedWriterThreads() {
        return super.getQueuedWriterThreads();
    }

    @Override
    protected Collection<Thread> getQueuedReaderThreads() {
        return super.getQueuedReaderThreads();
    }

    @Override
    protected Collection<Thread> getQueuedThreads() {
        return super.getQueuedThreads();
    }

    @Override
    public boolean hasWaiters(Condition condition) {
        return super.hasWaiters(condition);
    }

    @Override
    public int getWaitQueueLength(Condition condition) {
        return super.getWaitQueueLength(condition);
    }

    @Override
    protected Collection<Thread> getWaitingThreads(Condition condition) {
        return super.getWaitingThreads(condition);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
