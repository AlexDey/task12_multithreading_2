package Task1_ExplicitLock;

/**
 * @author Deyneka Oleksandr
 * @version 1.0 21 Dec 2018
 * Modify exercise 6 from previous presentation to use explicit Lock objects
 */
public class Application {

    public static void main(String[] args) {
        for (int i = 0; i<3; i++) {
            new SomeClass().start();
        }
    }
}
